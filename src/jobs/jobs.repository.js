const CrawlerModel = require('./jobs.model');

class JobsRepository {
    constructor() {
        this.model = CrawlerModel;
    }

    create(data) {
        return this.model.create(data);
    }
    
    save(data) {
        return data.save();
    } 
    
    count(condition) {
        return this.model.countDocuments(condition);
    }

    findOne(condition) {
        return this.model.findOne(condition);
    }

    findById(id) {
        return this.model.findById(id);
    }

    find(condition, page, limit) {
        if (page && limit) {
            return this.model.find(condition).skip(((page - 1) * limit)).limit(limit);
        } else {
            return this.model.find(condition);
        }
    }

}

module.exports = JobsRepository;
