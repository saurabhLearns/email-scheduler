const Controller = require('./jobs.controller');

module.exports = [
    { method: 'POST', path: '/v1/jobs', options: Controller.CreateJobs },
    { method: 'GET', path: '/v1/jobs', options: Controller.GetJobs },
    { method: 'GET', path: '/v1/jobs/{id}', options: Controller.GetJob },
];
  