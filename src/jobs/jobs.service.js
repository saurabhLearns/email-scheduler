const { log, PromiseHandler, DateParser } = require('../../utils/utility-functions')
const JobsRepository = require('./jobs.repository');

/**
 * Service function to create jobs
 * @param {*} request 
 * @param {*} h 
 * @returns 
 */
const CreateJobs = async (request, h) => {
    const jobsRepository = new JobsRepository();

    let promises = request.payload.map(async input => {
        input.execution_time = DateParser(input.time);
        return jobsRepository.create(input);
    })
    
    let [err, result] = await PromiseHandler(Promise.all(promises));
    if (err) {
        log.error("Error while adding jobs => ", err);
        return h.response({ message: 'Internal Server Error', data: null, statusCode: 500 }).code(500);
    }
    
    return h.response({ message: 'Created Successfully', data: result, statusCode: 201 }).code(201)
};

/**
 * Service function to get details of multiple jobs
 * @param {*} request 
 * @param {*} h 
 * @returns 
 */
const GetJobs = async (request, h) => {
    const jobsRepository = new JobsRepository();
    let [err, [count, jobs]] = await PromiseHandler(Promise.all([
        jobsRepository.count({}),
        jobsRepository.find({}, request.query.page, request.query.limit)
    ]));
    if (err) {
        log.error("Error while getting jobs => ", err);
        return h.response({ message: 'Internal Server Error', data: null, statusCode: 500 }).code(500);
    }
    return h.response({ message: 'Success', data: { count, jobs }, statusCode: 200 }).code(200);
};

/**
 * Service function to get details of single job
 * @param {*} request 
 * @param {*} h 
 * @returns
 */
const GetJob = async (request, h) => {
    const jobsRepository = new JobsRepository();
    
    let [err, job] = await PromiseHandler(jobsRepository.findById(request.params.id));
    if (err) {
        log.error("Error while getting a job => ", err);
        return h.response({ message: 'Internal Server Error', data: null, statusCode: 500 }).code(500)
    }
    
    return h.response({ message: 'Success', data: job, statusCode: 200 }).code(200)
}

module.exports = {
    CreateJobs,
    GetJobs,
    GetJob
}