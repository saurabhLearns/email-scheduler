const Service = require('./jobs.service');
const Validations = require('./jobs.validations');

const CreateJobs = {
    description: 'Schedule the jobs',
    tags: ['api'],
    validate: Validations.create_jobs,
    handler: async (request, h) => Service.CreateJobs(request, h)
};

const GetJobs = {
    description: 'Get all scheduled jobs',
    tags: ['api'],
    validate: Validations.get_jobs,
    handler: async (request, h) => Service.GetJobs(request, h)
};

const GetJob = {
    description: 'Get details of single job',
    tags: ['api'],
    validate: Validations.get_job,
    handler: async (request, h) => Service.GetJob(request, h)
};

module.exports = {
    CreateJobs,
    GetJobs,
    GetJob
}