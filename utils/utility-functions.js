const bunyan = require('bunyan');
const moment = require('moment');

const log = bunyan.createLogger({ name: "tagmango_scheduler" });

const PromiseHandler = (promise) => promise.then((data) => [null, data])
    .catch((err) => [err]); // custom promise handler

// this parser works on the assumption that the format for time will be always same as given in problem statement
// we can make it more dynamic if we knew what kind of inputs are going to be available here for time field
// for now its totally static on the basis of input format given in problem statement
const DateParser = (date) => {
    if (date == 'now') {
        return Date.now();
    }

    // we are assuming that the date will be always in hours when we pass 'x hours' later in payload
    if (date.includes('later')) {
        date = date.split(' ')
        return (Date.now() + (parseInt(date[0]) * 60 * 60 * 1000))
    }

    // matching date inside bracket with regex.
    // Assuming that input time will be in IST timezone, we are converting it into UTC format and utc timezone 
    // so that operation can happen according to standard timezones (UTC timezone)
    // conversion happens inside mongo automatically
    if (date.includes('on a particular time')) {
        let rawDate = date.match(/\( Ex- (.*?)\)/);
        let [dateWithMonth, year, time] = rawDate[1].split(',')
        let [day, month] = dateWithMonth.split(' ')
        time = moment(time, 'hh:mm A').format('HH:mm:ss')
        day = day.match(/\d/g).join("");
        let finalDate = `${month} ${day}, ${year} ${time}`;
        return finalDate;
    }
}

module.exports = {
    log,
    PromiseHandler,
    DateParser
}
